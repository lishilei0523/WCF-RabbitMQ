﻿using RabbitMQ.WCF.AppService.Implements;
using System;
using System.ServiceModel;

namespace RabbitMQ.WCF.AppService.Host
{
    class Program
    {
        static void Main()
        {
            ServiceHost orderSvcHost = new ServiceHost(typeof(OrderContract));
            orderSvcHost.Open();

            Console.WriteLine("服务已启动...");
            Console.ReadKey();

            orderSvcHost.Close();
        }
    }
}
